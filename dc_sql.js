const mysql = require('mysql2/promise');
var connection;

module.exports = {
    initMysql: async function() {
        connection = await mysql.createConnection({
            host: 'localhost',
            user: 'root',
            password: 'password',
            database: 'decios'
        });
    },

    getConnection: function() {
        return connection;
    },
    
    closeMysql: function() {
        connection.end();
    }
}

# Delivery Documents Table Patch

This is a small collection of scrips used to correct an issue with the delivery_documents table in decios.

As it stands, each delivery document in the table can cover multiple deliveries despite the fact that only one delivery is ever referenced in the delivery_id column of the table.

This project fixes this issue by going through the deliveries and their documents and inserting a new entry in the delivery_documents table for each delivery that the document applies to.

## Requirements/Setup

* Download and install Node.js
   * I made this using version v12.6.0. This is not tested with future/past versions
* Ensure you have proper database credentials for your desired Decios database set in the dc_sql.js file

## How to Use

1. Run the following query and export the results as a JSON file named **delivery_data.json** and place the file in the same directory as this script
   * I did this through MySQL workbench
   * This query may take a while to run, considering the large amount of data being selected and joined (2 hours and 2 minutes on my desktop)

```sql
SELECT ds.delivery_id, deliveries.pickup_date, delivery_documents.id AS document_id, delivery_documents.file_name, ds.administrative_area, ds.street_number, ds.route, ds.suite, ds.locality, ds.country
FROM (SELECT delivery_stops.delivery_id, delivery_stops.administrative_area, delivery_stops.street_number, delivery_stops.route, delivery_stops.suite, delivery_stops.locality, delivery_stops.country FROM delivery_stops WHERE delivery_stops.address_type = 2) ds
JOIN deliveries ON ds.delivery_id = deliveries.id
LEFT JOIN delivery_documents ON deliveries.id = delivery_documents.delivery_id
```

2. Run "**node index.js**"
   * This may take a while to run. This script is matching every existing delivery document with the deliveries that do not technically have a document set in the delivery_documents table
3. After index.js ends execution, the results will be saved to a file named matched_data.json
   * This file is a list of every document-less delivery matched with its "parent" delivery which does contain a document
   * **Note:** If a delivery does not have a document and a proper parent delivery was not found, it will not be in this file.
4. Run "**node push_changes.js**"
   * This should run much quicker than index.js
   * Upon completion, this should save a file named results.sql to the same directory as the rest of the script
   * This sql file contains individual INSERT statements for each new entry in the delivery_documents table.
      * This was originally one mass insert statement but I couldn't get my MySQL server to run the single statement without timing out (even after messing with the config)
5. Go back to your MySQL server and run an import using this .sql file.
   * I ran this through MySQL workbench; it took roughly 10 minutes to complete.

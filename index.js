const fs = require('fs');

const readline = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout
});

console.log('Program started!');

var deliveriesWithDocuments = {};
var deliveriesWithoutDocuments = [];
var matchingDeliveriesFound = 0;

console.log('Reading data from file...');
var rawData = fs.readFileSync('delivery_data.json');

console.log('Parsing JSON text...');
var deliveries = JSON.parse(rawData);

console.log('Sorting deliveries with/without a corresponding delivery document...');
for (let i = 0; i < deliveries.length; i++) {
    var delivery = deliveries[i];

    if (delivery.file_name === null) {
        deliveriesWithoutDocuments.push(i);
    } else {
        if (typeof deliveriesWithDocuments[delivery.delivery_id] !== 'undefined') {
            deliveriesWithDocuments[delivery.delivery_id].parents.push(i);
        } else {
            deliveriesWithDocuments[delivery.delivery_id] = {
                parents: [i],
                children: []
            };
        }
    }
}

console.log('Deliveries sorted');

console.log('Matching deliveries without documents with matching deliveries that do have documents...');
for(let i = 0; i < deliveriesWithoutDocuments.length; i++) {
    let deliveryNoDocumentId = deliveriesWithoutDocuments[i];
    let deliveryNoDocument = deliveries[deliveryNoDocumentId];
    
    for (deliveryWithDocumentKey in deliveriesWithDocuments) {
        var parent0Id = deliveriesWithDocuments[deliveryWithDocumentKey].parents[0];
        var parent0 = deliveries[parent0Id];

        //if two deliveries have the same pickup date, street number, administrative area, country, and route then match them
        if (parent0.pickup_date == deliveryNoDocument.pickup_date && parent0.street_number == deliveryNoDocument.street_number && parent0.administrative_area == deliveryNoDocument.administrative_area && parent0.country == deliveryNoDocument.country && parent0.route == deliveryNoDocument.route) {
            console.log('Found matching deliveries! Adding to children');
            deliveriesWithDocuments[deliveryWithDocumentKey].children.push(deliveryNoDocumentId);
            matchingDeliveriesFound++;
        }
    }
    if (i % 1000 == 0) {
        console.log("Currently on deliveryWithoutDocument number: " + i);
    }
}

console.log('Finished matching deliveries!');
console.log('Found ' + matchingDeliveriesFound + ' document-less deliveries to match.');

var matchedDataAsJson = JSON.stringify(deliveriesWithDocuments);

fs.writeFile('./matched_data.json', matchedDataAsJson, function(error) {
    if (error) {
        console.log(error);
    } else {
        console.log('Successfully wrote results to file: matched_data.json');
    }

    console.log('Closing fs connection');
    fs.close(2, () => {});

    console.log(' - Process complete -');
});


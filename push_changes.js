var mysql = require('./dc_sql');
const fs = require('fs');
const readline = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout
});

console.log('Reading data from file...');
var rawMatchedData = fs.readFileSync('matched_data.json');
var rawDeliveryData = fs.readFileSync('delivery_data.json');

console.log('Parsing JSON text...');
var matches = JSON.parse(rawMatchedData);
var deliveries = JSON.parse(rawDeliveryData);
var allDocumentData = {};
async function startProgram() {
    await mysql.initMysql();
    var connection = mysql.getConnection();
    
    const [docRows, docFields] = await connection.execute('SELECT * FROM delivery_documents;', []);

    console.log('Closing MySQL connection');
    mysql.closeMysql();

    docRows.forEach(function(row) {
        let docId = row.id;
        allDocumentData[docId] = row;
    });

    let finalSqlInsert = '';

    var count = 0;
    for (var property in matches) {
        if (matches.hasOwnProperty(property)) {
            if (count % 1000 == 0) {
                console.log('On match number: ' + count);
            }
            count++;
            if (!isNaN(property)) { //is a number
                let delivId = parseInt(property);
                let matchData = matches[delivId];

                matchData.children.forEach(function(child) {
                    matchData.parents.forEach(function(parent) {
                        let parentData = deliveries[parent];
                        let childData = deliveries[child];
                        let documentData = allDocumentData[parentData.document_id];

                        //Data Needed
                        //(id (auto increment), stop_id, address_type, file_name, created_at, updated_at, deleted_at, document_type, not_found, delivery_id)

                        let stopId = documentData.stop_id;
                        let addressType = documentData.address_type;
                        let fileName = documentData.file_name;
                        let documentType = documentData.document_type;
                        let notFound = documentData.not_found;
                        let deliveryId = childData.delivery_id;

                        let created_at = null, updated_at = null, deleted_at = null;
                        if (documentData.created_at != null) {
                            let d = new Date(documentData.created_at);
                            created_at = '\'' + d.getFullYear() + '-' + ('0' + (d.getMonth() + 1)).slice(-2) + '-' + ('0' + d.getDate()).slice(-2) + ' ' + ('0' + d.getHours()).slice(-2) + ':' + ('0' + d.getMinutes()).slice(-2) + ':' + ('0' + d.getSeconds()).slice(-2) + '\'';
                        }
                        if (documentData.updated_at != null) {
                            let d = new Date(documentData.updated_at);
                            updated_at = '\'' + d.getFullYear() + '-' + ('0' + (d.getMonth() + 1)).slice(-2) + '-' + ('0' + d.getDate()).slice(-2) + ' ' + ('0' + d.getHours()).slice(-2) + ':' + ('0' + d.getMinutes()).slice(-2) + ':' + ('0' + d.getSeconds()).slice(-2) + '\'';
                        }
                        if (documentData.deleted_at != null) {
                            let d = new Date(documentData.deleted_at);
                            deleted_at = '\'' + d.getFullYear() + '-' + ('0' + (d.getMonth() + 1)).slice(-2) + '-' + ('0' + d.getDate()).slice(-2) + ' ' + ('0' + d.getHours()).slice(-2) + ':' + ('0' + d.getMinutes()).slice(-2) + ':' + ('0' + d.getSeconds()).slice(-2) + '\'';
                        }

                        finalSqlInsert += 'INSERT INTO delivery_documents VALUES (null, ' + stopId + ', ' + addressType + ', \'' + fileName + '\', ' + created_at + ', ' + updated_at + ', ' + deleted_at + ', ' + documentType + ', ' + notFound + ', ' + deliveryId + '); ';
                    });
                });
            }
        }
    }

    console.log('Finished creating INSERT statement.');
    console.log('Saving result to results.sql');
    fs.writeFile('./results.sql', finalSqlInsert, function(error) {
        if (error)
            console.log(error);
        else
            console.log('Successfully wrote sql to file');
        
        console.log('Closing fs');
        fs.close(2, () => {});

        console.log('Program complete!');
    });
}

startProgram();